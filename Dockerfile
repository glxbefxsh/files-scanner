FROM  maven:3.8.8-eclipse-temurin-17
COPY . .
ENV AWS_ACCESS_KEY_ID=XXX
ENV AWS_SECRET_ACCESS_KEY=XXX
ENV AWS_SESSION_TOKEN=XXX
RUN mvn clean install
CMD ["java", "-jar", "target/demo-0.0.1-SNAPSHOT.jar"]

