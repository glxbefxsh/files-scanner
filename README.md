# Projet de Génie Logiciel

**Eliott Bourrigan & Tanguy Legrand**

## I. Présentation de l’application

### Description générale

L’application *Files Scanner* est une application backend permettant de scanner un répertoire de fichiers local ou un bucket S3. L’utilisateur peut choisir les paramètres suivants : Le nombre de fichiers maximal à scanner, la profondeur maximale à scanner dans l’arborescence des fichiers, le filtre sur le nom du fichiers (une chaîne de caractères qui devra être contenue dans le nom d’un fichier pour que celui-ci soit scanné) et le filtre sur l’extension du fichier.

Le répertoire GitLab contenant le projet est disponible [ici](https://gitlab.com/glxbefxsh/files-scanner).

### Technologies utilisées

L’application est développée avec Java 17 et Spring Boot 3.2, et les tests utilisent Junit 5.

La base de données est une base de données PostgreSQL.

L’ensemble de l’application est lancée avec Docker Compose.

### Fonctionnalités manquantes

Les fonctionnalités suivantes n’ont pas pu être implémentées pour l’instant, et feront probablement l’objet d’une version ultérieure :

- Le déploiement automatique (*CD*) de l’application sur AWS n’est pas implémenté.
- Les statistiques de scan (durée de scan moyen par fichier et par répertoire) ne sont pas calculées.
- La comparaison de scans n’est pas implémentée.
- Un seul *design pattern* est implémenté pour l’instant. A l’avenir, le calcul de la durée de scan par fichier et par répertoire pourrait être une opération implémentée au moyen d’un pattern *Composite*.
- Les tests ne couvrent qu’une petite partie de l’application.

## II. Utilisation de l’application

### Installation et lancement

Pour utiliser l’application, le plus simple est d’utiliser Docker Compose. Une fois que le Docker engine est en route, ouvrez un terminal, naviguez jusqu’au dossier racine de l’application, puis entrez simplement la commande suivante : 

```bash
docker compose up
```

Docker va alors importer les images nécessaires, puis les lancer. Le processus d’installation est susceptible de prendre jusqu’à 3 minutes. A la fin de celui-ci, l’API de l’application est accessible via le port 5000 du *localhost*.

Lors de la création d’un scan ou d’un *replay* de scan, les arguments correspondant aux deux filtres (nom et extension) sont requis, mais si vous entrez la chaîne de caractères `null`, aucun filtre ne sera appliqué.

### Utilisation en local

Pour scanner un répertoire local, il suffit d’entrer l’URL `[https://localhost:5000/swagger](https://localhost:5000/swagger)` sur votre navigateur pour accéder au *swagger* de l’API, d’entrer `Local` dans le champ `strategyName` et le répertoire à scanner dans le champ `path`, dans l’endpoint `runScan`. Ce scan est alors enregistré dans la base de données, son ID vous est donné, pour que vous puissiez accéder aux métadonnées du scan via l’endpoint `getScan` ou aux fichiers scannés via `getFiles`. 

Attention, si vous utilisez docker, les répertoires locaux sont donc ceux du container qui fait tourner l’application, les répertoires de votre machine personnelle ne sont donc pas directement accessibles.

### Utilisation avec S3

Pour scanner un bucket *S3* avec l’application *FIles Scanner*, assurez-vous d’abord d’avoir entré vos *Credentials* AWS en variable d’environnement. Si vous utilisez Docker, vous devrez peut-être entrer vos identifiants AWS dans les lignes 3 à 5 du Dockerfile de l’application pour avoir accès à votre bucket S3. Assurez-vous aussi que votre utilisateur AWS a accès au bucket en question, ou que celui-ci est public.

Ensuite il suffit d’entrer l’URL `[https://localhost:5000/swagger](https://localhost:5000/swagger)` sur votre navigateur pour accéder au *swagger* de l’API, d’entrer `S3` dans le champ `strategyName` et le nom du bucket S3 à scanner dans le champ `path`, dans l’endpoint `runScan`. Ce scan est alors enregistré dans la base de données, son ID vous est donné, pour que vous puissiez accéder aux métadonnées du scan via l’endpoint `getScan` ou aux fichiers scannés via `getFiles`. 

## III. Détail de l’application

### Architecture du projet

```bash
files-scanner
│   .gitignore
│   .gitlab-ci.yml
│   docker-compose.yml
│   Dockerfile
│   pom.xml
│   README.md
└───src
    ├───main
    │   ├───java
    │   │   └───fr
    │   │       └───ensai
    │   │           └───filescanner
    │   │               │   Application.java
    │   │               ├───controller
    │   │               │       ScanController.java
    │   │               ├───dto
    │   │               │       FileDataDTO.java
    │   │               │       ScanDataDTO.java
    │   │               ├───model
    │   │               │       FileData.java
    │   │               │       ScanData.java
    │   │               ├───repository
    │   │               │       FileDataRepository.java
    │   │               │       ScanDataRepository.java
    │   │               ├───scan
    │   │               │       LocalStrategy.java
    │   │               │       S3Strategy.java
    │   │               │       ScanStrategy.java
    │   │               └───service
    │   │                       ScanService.java
    │   └───resources
    │           application-integrationtest.properties
    │           application.properties
    └───test
        └───java
            └───fr
                └───ensai
                    └───filescanner
                            ApplicationTests.java
```

### Détail des classes *Model* du projet

L’architecture de l’application suit le modèle MVC (pour *Model Vue Controller).* La partie *Vue* n’est évidemment pas implémentée puisque *Files Scanner* est une application backend uniquement. Voici dans un premier temps le détail des classes de la partie *Model* :

- La classe `FileData` représente les métadonnées d’un fichier scanné. Une instance de cette classe représente un seul scan d’un seul fichier, et donc il peut exister plusieurs instances représentant le même fichier s’il a été scanné par plusieurs scans. Elle possède les attributs `fileId`, `name`, `modifDate`, `size`, `extension`, `fullPath` et `scanId`. Tous ces attributs sont privés, et sont associés à des *Getters* et des *Setters* générés automatiquement par la libraire *Lombok.* Cette même librairie ajoute automatiquement un constructeur spécifiant tous les attributs (*AllArgsConstructor*) et un constructeur n’en spécifiant aucun, qui leur attribue la valeur *null* (*NoArgsConstructor*). La librairie *Jakarta* permet quand à elle de lier automatiquement chacun de ces attributs à une colonne de la table SQL associée à la classe.
- La classe `ScanData` représente les métadonnées d’un scan. Elle possède les attributs `scanId`, `maxFileCount`, `maxDepth`, `fileNameFilter`, `fileTypeFilter`, `scanDate`, `scanTime`, `directoryPath` et `strategyName`. Comme précédemment, les *Getters*, *Setters*, les constructeurs et les associations avec la table correspondant à la classe sont gérées automatiquement par *Lombok* et *Jakarta*.

### Stratégies de scan

Pour l’implémentation du scan de fichier, le design pattern *Strategy* est utilisé. L’interface `ScanStrategy` possède une méthode abstraite `scanDirectory`, qui est implémentée différemment dans les deux classes qui implémentent `ScanStrategy`. Ces deux classes sont `S3Strategy` et `LocalScanStrategy`, qui permettent respectivement de scanner un bucket S3 et un répertoire local.

### Détails des endpoints de l’API

1. **Endpoint `/hello`** :
    - **Méthode** : GET
    - **Description** : Il s'agit d'un test simple qui retourne la chaîne de caractères "Hello World". Ce endpoint ne prend aucun paramètre et sert généralement à vérifier que le contrôleur est correctement configuré et accessible.
2. **Endpoint `/runScan`** :
    - **Méthode** : GET
    - **Description** : Cet endpoint déclenche un scan en fonction de plusieurs paramètres spécifiés par l'utilisateur : **`strategyName`** (*S3* ou *Local*), **`path`** (le chemin du répertoire à scanner ou le nom du bucket S3), **`fileNameFilter`** (un filtre sur les noms de fichiers), **`fileTypeFilter`** (un filtre sur les types de fichiers), **`maxFiles`** (le nombre maximum de fichiers à scanner), et **`maxDepth`** (la profondeur maximale de la recherche dans l'arborescence des dossiers). Les filtres peuvent être définis comme "*null*" pour ne pas les appliquer. L'ID du scan effectué est retourné dans la réponse.
3. **Endpoint `/replayScan`** :
    - **Méthode** : GET
    - **Description** : Cet endpoint permet de rejouer un scan précédemment effectué en utilisant son ID (**`scanId`**), tout en permettant de redéfinir certains paramètres tels que **`maxFiles`**, **`maxDepth`**, **`fileNameFilter`**, et **`fileTypeFilter`**. La méthode récupère d'abord les informations du scan original (nom de la stratégie et chemin) à partir de son ID, puis lance un nouveau scan avec les paramètres spécifiés.
4. **Endpoint `/getScans`** :
    - **Méthode** : GET
    - **Description** : Cet endpoint retourne la liste de tous les scans effectués, en utilisant le service de scan pour récupérer ces informations. Aucun paramètre n'est nécessaire pour cette requête.
5. **Endpoint `/getFiles`** :
    - **Méthode** : GET
    - **Description** : Cet endpoint permet de récupérer la liste des fichiers issus d'un scan spécifique, identifié par son ID (**`scanId`**). Le service de scan est utilisé pour obtenir ces informations, et les résultats sont retournés sous forme de chaîne de caractères.