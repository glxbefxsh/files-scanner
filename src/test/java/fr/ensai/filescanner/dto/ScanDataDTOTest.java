package fr.ensai.filescanner.dto;

import org.junit.jupiter.api.Test;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScanDataDTOTest {

    @Test
    public void testConstructorAndGetters() {
        // Prepare test data
        Long scanId = 1L;
        String parentFolderPath = "/test/path";
        String strategyName = "TestStrategy";
        Integer maxFiles = 100;
        Integer maxDepth = 5;
        Duration duration = Duration.ofHours(1);
        String nameFilter = "*.java";
        List<String> typeFilter = Arrays.asList("txt", "java");

        // Create an instance of ScanDataDTO using the constructor
        ScanDataDTO scanDataDTO = new ScanDataDTO(scanId, parentFolderPath, strategyName, maxFiles, maxDepth, duration, nameFilter, typeFilter);

        // Verify that getters return the correct values
        assertEquals(scanId, scanDataDTO.getScanId());
        assertEquals(parentFolderPath, scanDataDTO.getParentFolderPath());
        assertEquals(strategyName, scanDataDTO.getStrategyName());
        assertEquals(maxFiles, scanDataDTO.getMaxFiles());
        assertEquals(maxDepth, scanDataDTO.getMaxDepth());
        assertEquals(duration, scanDataDTO.getDuration());
        assertEquals(nameFilter, scanDataDTO.getNameFilter());
        assertEquals(typeFilter, scanDataDTO.getTypeFilter());
    }
}
