package fr.ensai.filescanner.dto;

import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileDataDTOTest {

    @Test
    public void testGettersAndSetters() {
        // Create a LocalDateTime object for testing
        LocalDateTime testDate = LocalDateTime.now();
        
        // Create an instance of FileDataDTO and set values
        FileDataDTO fileDataDTO = new FileDataDTO();
        fileDataDTO.setFileId(1L);
        fileDataDTO.setName("TestFile");
        fileDataDTO.setFullPath("/test/path");
        fileDataDTO.setModifDate(testDate);
        fileDataDTO.setSize(1024L);
        fileDataDTO.setExtension("txt");
        fileDataDTO.setScanId(2L);

        // Verify that getters return the correct values
        assertEquals(1L, fileDataDTO.getFileId());
        assertEquals("TestFile", fileDataDTO.getName());
        assertEquals("/test/path", fileDataDTO.getFullPath());
        assertEquals(testDate, fileDataDTO.getModifDate());
        assertEquals(1024L, fileDataDTO.getSize());
        assertEquals("txt", fileDataDTO.getExtension());
        assertEquals(2L, fileDataDTO.getScanId());
    }
}
