package fr.ensai.filescanner.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileDataTest {

    @Test
    public void testSetAndGetFileName() {
        FileData fileData = new FileData();
        fileData.setName("test.txt");
        assertEquals("test.txt", fileData.getName());
    }

    // Repeat the above test method for each getter/setter pair in FileData
    @Test
    public void testSetAndGetScanId() {
        FileData fileData = new FileData();
        fileData.setScanId(1L);
        assertEquals(1L, fileData.getScanId());
    }

    @Test
    public void testSetAndGetSize() {
        FileData fileData = new FileData();
        fileData.setSize(100L);
        assertEquals(100L, fileData.getSize());
    }

    @Test
    public void testSetAndGetPath() {
        FileData fileData = new FileData();
        fileData.setFullPath("C:/test.txt");
        assertEquals("C:/test.txt", fileData.getFullPath());
    }

    // test no args constructor
    @Test
    public void testNoArgsConstructor() {
        FileData fileData = new FileData();
        assertEquals(null, fileData.getName());
        assertEquals(null, fileData.getScanId());
        assertEquals(null, fileData.getSize());
        assertEquals(null, fileData.getFullPath());
    }

    // test all args constructor
    @Test
    public void testAllArgsConstructor() {
        FileData fileData = new FileData("test.txt", "C:/test.txt", null, 100L, "txt", 1L);
        assertEquals("test.txt", fileData.getName());
        assertEquals(1L, fileData.getScanId());
        assertEquals(100L, fileData.getSize());
        assertEquals("C:/test.txt", fileData.getFullPath());
    }
}