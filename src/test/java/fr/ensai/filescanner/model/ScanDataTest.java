package fr.ensai.filescanner.model;

import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ScanDataTest {
   
      // Repeat the above test method for each getter/setter pair in ScanData
      @Test
      public void testSetAndGetScanId() {
         ScanData scanData = new ScanData();
         scanData.setScanId(1L);
         assertEquals(1L, scanData.getScanId());
      }
   
      @Test
      public void testSetAndGetScanType() {
         ScanData scanData = new ScanData();
         scanData.setStrategyName("Full");
         assertEquals("Full", scanData.getStrategyName());
      }
   
      @Test
      public void testSetAndGetScanDate() {
         ScanData scanData = new ScanData();
         LocalDate mockDate = LocalDate.of(2021, 1, 1);
         scanData.setScanDate(mockDate);
         assertEquals(mockDate, scanData.getScanDate());
      }

      @Test
      public void testSetAndGetMaxFiles() {
         ScanData scanData = new ScanData();
         scanData.setMaxFileCount(100);
         assertEquals(100, scanData.getMaxFileCount());
      }

      @Test
      public void testSetAndGetMaxDepth() {
         ScanData scanData = new ScanData();
         scanData.setMaxDepth(5);
         assertEquals(5, scanData.getMaxDepth());
      }

      // test no args constructor
      @Test
      public void testNoArgsConstructor() {
         ScanData scanData = new ScanData();
         assertEquals(null, scanData.getScanId());
         assertEquals(null, scanData.getMaxFileCount());
         assertEquals(null, scanData.getMaxDepth());
         assertEquals(null, scanData.getFileNameFilter());
         assertEquals(null, scanData.getFileTypeFilter());
         assertEquals(null, scanData.getScanDate());
         assertEquals(null, scanData.getScanTime());
         assertEquals(null, scanData.getDirectoryPath());
         assertEquals(null, scanData.getStrategyName());
      }

      // test all args constructor
      @Test
      public void testAllArgsConstructor() {
         LocalDate mockDate = LocalDate.of(2021, 1, 1);
         ScanData scanData = new ScanData(1L, 100, 5, "test", "txt", mockDate, 100L, "C:/test", "Full");
         assertEquals(1L, scanData.getScanId());
         assertEquals(100, scanData.getMaxFileCount());
         assertEquals(5, scanData.getMaxDepth());
         assertEquals("test", scanData.getFileNameFilter());
         assertEquals("txt", scanData.getFileTypeFilter());
         assertEquals(mockDate, scanData.getScanDate());
         assertEquals(100L, scanData.getScanTime());
         assertEquals("C:/test", scanData.getDirectoryPath());
         assertEquals("Full", scanData.getStrategyName());
      }
}
