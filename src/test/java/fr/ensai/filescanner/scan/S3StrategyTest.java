package fr.ensai.filescanner.scan;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class S3StrategyTest {

   @Test
   public void testFileMatchesFilters() {
      String mockFileName = "myfile.txt";

       // Test case 1: File name matches the filter
       assertTrue(S3Strategy.fileMatchesFilters(mockFileName, "ile", "txt"));

         // Test case 2: File name does not match the filter
         assertFalse(S3Strategy.fileMatchesFilters(mockFileName, "ile", "jpg"));

         // Test case 3: File name matches the filter
         assertFalse(S3Strategy.fileMatchesFilters(mockFileName, "notmyfile", "txt"));

         // Test case 4: File name does not match any filter
         assertFalse(S3Strategy.fileMatchesFilters(mockFileName, "notmyfile", "jpg"));
         
   }
}