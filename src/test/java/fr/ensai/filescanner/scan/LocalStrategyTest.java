package fr.ensai.filescanner.scan;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.File;

class LocalStrategyTest {

   @Test
   void testMatchesFilter() {
      // Create an instance of LocalScanStrategy
      LocalStrategy strategy = new LocalStrategy();
      File mockFile = new File("myfile.txt");

      // Test case 1: File name matches the filter
      assertTrue(strategy.fileMatchesFilters(mockFile, "ile", "txt"));

      // Test case 2: File name does not match the filter
      assertFalse(strategy.fileMatchesFilters(mockFile, "ile", "jpg"));

      // Test case 3: File name matches the filter
      assertTrue(strategy.fileMatchesFilters(mockFile, "notmyfile", "txt"));

      // Test case 4: File name does not match any filter
      assertFalse(strategy.fileMatchesFilters(mockFile, "notmyfile", "jpg"));
   }
}
