package fr.ensai.filescanner.controller;
import fr.ensai.filescanner.service.ScanService;
import fr.ensai.filescanner.model.ScanData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class ScanController {
    
    @Autowired
    public ScanService scanService;

    // Test: Hello World
    @GetMapping("/hello")
    public String hello() {
        return "Hello World";
    }

    // start scan. specify type, path, maxfiles and maxdepth
    @GetMapping("/runScan")
    public String runScan(String strategyName, String path, String fileNameFilter, String fileTypeFilter, Integer maxFiles, Integer maxDepth) {
        if ("null".equals(fileNameFilter)) {
            fileNameFilter = null;
        }
        if ("null".equals(fileTypeFilter)) {
            fileTypeFilter = null;
        }
        Long newScanId = scanService.runScan(strategyName, path, fileNameFilter, fileTypeFilter, maxFiles, maxDepth);
        return "Scan completed and saved in database with ID " + newScanId;
    }

    // replay scan using scanid and redefining maxfiles, maxdepth, filenamefilter and filetypefilter
    @GetMapping("/replayScan")
    public String replayScan(Long scanId, Integer maxFiles, Integer maxDepth, String fileNameFilter, String fileTypeFilter) {
        if ("null".equals(fileNameFilter)) {
            fileNameFilter = null;
        }
        if ("null".equals(fileTypeFilter)) {
            fileTypeFilter = null;
        }

        // get strategy name and path from original scan using scanId
        ScanData originalScan = scanService.getScan(scanId);
        String strategyName = originalScan.getStrategyName();
        String path = originalScan.getDirectoryPath();
        

        Long newScanId = scanService.runScan(strategyName, path, fileNameFilter, fileTypeFilter, maxFiles, maxDepth);
        return "Scan completed and saved in database with ID " + newScanId;
    }

    // get all scans
    @GetMapping("/getScans")
    public String getScans() {
        return scanService.getScans().toString();
    }

    // get files from scan
    @GetMapping("/getFiles")
    public String getFiles(Long scanId) {
        return scanService.getFiles(scanId).toString();
    }

    // Test: Run scan
    @GetMapping("/runTestScan")
    public String runTestScan() {
        Long newScanId = scanService.runScan("S3", "eliottandtanguyfiles", null, null, 10, 100);
        return "Scan completed and saved in database with ID " + newScanId;
    }
}
