package fr.ensai.filescanner.scan;

public interface ScanStrategy {
    Long scanDirectory(String parentFolderPath,
                                 String nameFilter,
                                 String typeFilter,
                                 Integer maxDepth,
                                 Integer maxFiles);
}
