package fr.ensai.filescanner.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.LocalDate;


@Entity
@Table(name = "scans")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScanData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "scanid")
    private Long scanId;

    @Column(name = "maxfilecount")
    private Integer maxFileCount;

    @Column(name = "maxdepth")
    private Integer maxDepth;

    @Column(name = "filenamefilter")
    private String fileNameFilter;

    @Column(name = "filetypefilter")
    private String fileTypeFilter;

    @Column(name = "scandate")
    private LocalDate scanDate;

    @Column(name = "scantime")
    private Long scanTime;

    @Column(name = "directorypath")
    private String directoryPath;
    
    @Column(name = "strategyname")
    private String strategyName;
}
