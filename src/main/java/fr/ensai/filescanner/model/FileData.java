package fr.ensai.filescanner.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "files")
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FileData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fileid")
    private Long fileId;

    @Column(name = "name")
    private String name;

    @Column(name = "modifdate")
    private LocalDateTime modifDate;

    @Column(name = "size")
    private Long size;

    @Column(name = "extension")
    private String extension;

    @Column(name = "fullpath")
    private String fullPath;

    @Column(name = "scanid")
    private Long scanId;

    public FileData(String name, String fullPath, LocalDateTime modifDate, Long size, String extension, Long scanId){
        this.fileId = null;
        this.name = name;
        this.modifDate = modifDate;
        this.size = size;
        this.extension = extension;
        this.fullPath = fullPath;
        this.scanId = scanId;
    }
}
