package fr.ensai.filescanner.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ensai.filescanner.model.ScanData;
import fr.ensai.filescanner.model.FileData;
import fr.ensai.filescanner.repository.ScanDataRepository;
import fr.ensai.filescanner.scan.LocalStrategy;
import fr.ensai.filescanner.scan.S3Strategy;
import fr.ensai.filescanner.repository.FileDataRepository;

@Service
public class ScanService {

    @Autowired
    private LocalStrategy localStrategy;

    @Autowired
    private S3Strategy s3Strategy;

    @Autowired
    private ScanDataRepository scanDataRepository;

    @Autowired
    private FileDataRepository fileDataRepository;

    public Iterable<ScanData> getScans() {
        return scanDataRepository.findAll();
    }

    public ScanData getScan(Long scanId) {
        return scanDataRepository.findById(scanId).orElse(null);
    }

    public Iterable<FileData> getFiles(Long scanId) {
        return fileDataRepository.findByScanId(scanId);
    }

    public Long runScan(String strategyName,
                            String directoryPath,
                            String fileNameFilter,
                            String fileTypeFilter,
                            Integer maxDepth,
                            Integer maxFiles) {
        if (strategyName == null || directoryPath == null) {
            throw new IllegalArgumentException("Strategy name and directory path are required");
        }

        if (strategyName.equals("S3")) {
            return this.s3Strategy.scanDirectory("eliottandtanguyfiles", fileNameFilter, fileTypeFilter, maxDepth, maxFiles);
        } else if (strategyName.equals("Local")) {
            return this.localStrategy.scanDirectory(directoryPath, fileNameFilter, fileTypeFilter, maxDepth, maxFiles);
        } else {
            throw new IllegalArgumentException("Invalid strategy name");
        }
    }
}
