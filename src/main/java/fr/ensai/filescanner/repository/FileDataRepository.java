package fr.ensai.filescanner.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.ensai.filescanner.model.FileData;

@Repository
public interface FileDataRepository extends CrudRepository<FileData, Long> {
    public Iterable<FileData> findByName(String name);
    public Iterable<FileData> findByScanId(Long scanId);
}
