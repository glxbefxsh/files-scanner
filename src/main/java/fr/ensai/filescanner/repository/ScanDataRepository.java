package fr.ensai.filescanner.repository;

import java.time.LocalDateTime;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.ensai.filescanner.model.ScanData;

@Repository
public interface ScanDataRepository extends CrudRepository<ScanData, Long> {
    public Iterable<ScanData> findByScanDate(LocalDateTime scanDate);
}
