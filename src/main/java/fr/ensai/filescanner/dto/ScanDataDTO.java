package fr.ensai.filescanner.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.time.Duration;

@Getter
@Setter
@AllArgsConstructor
public class ScanDataDTO {
    private Long scanId;
    private String parentFolderPath;
    private String strategyName;
    private Integer maxFiles;
    private Integer maxDepth;
    private Duration duration;
    private String nameFilter;
    private List<String> typeFilter;
}
