package fr.ensai.filescanner.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FileDataDTO {
    
    private Long fileId;
    private String name;
    private String fullPath;
    private LocalDateTime modifDate;
    private Long size;
    private String extension;
    private Long scanId;
}
